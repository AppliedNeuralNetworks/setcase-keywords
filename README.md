# SETCASE-KEYWORDS

## USAGE
`sk list-of-keywords.txt <text-in.txt >text-out.txt`

## PURPOSE

`sk` will alter the case of keywords in a given text to your preference.
Keywords are matched case-insensitively and word boundaries are respected.
The list of keywords is provided as the sole mandatory argument, and
refers to a file in which keywords are provided, one per line.  The text
to be altered is delivered on stdin/stdout, in the style of
`awk`, `sed`, `grep`, and others.

## EXAMPLE

You wish to make three alterations to a program you have written. You want to
change all occurrences of a function called **foo** to title case (**Foo**); you
want to change all occurrences of the constant **BAR** to lowercase
(**bar**); and you wish to rename a variable **quux** to **quUX**.

The list of keywords is an arbitrarily named text file (the file name is given
as an argument to the program) and contains the keywords, one per line, in the
desired case. For this example, the file would contain:
```
    Foo
    bar
    quUX
```
When `sk` is executed, it replaces *all* case-insensitive matches for these keywords
with the keyword string you specified in the file.

## SIDE EFFECTS and IMPLICATIONS

Note that when the keywords file is read, whitespace is striped from
the beginning and ending of each line -- but not otherwise. This makes
indentation irrelevant. Therefore **"   cat "** is the same as **cat**.

Internal whitespace preservation leads to interesting possibilities.  If
an entry for **Happy Birthday** is specified, **happy birthday** will be
matched and transformed.  Meanwhile, because word-boundaries are respected
for the whole keyword string, the phrase **chappy birthdays** in the text
would be unaffected.
